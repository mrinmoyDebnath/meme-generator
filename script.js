const memeRequest = document.querySelector('#meme-form');
const memeContainer = document.querySelector('.meme-container');

memeRequest.addEventListener('submit', event=>{
    event.preventDefault()
    const imgUrl = event.currentTarget.elements[0].value;
    const textTop = event.currentTarget.elements[1].value;
    const textBottom = event.currentTarget.elements[2].value;
    if(!imgUrl) alert('URL is required');
    else{
        getMeme(imgUrl, textTop, textBottom);
    }
})

function getMeme(imgUrl, textTop, textBottom){
    fetch(imgUrl)
    .then(response=>response.blob())
    .then(blob => {
        const objectURL = URL.createObjectURL(blob);
        const newDiv = document.createElement('div');
        const hoverDiv = document.createElement('div');
        newDiv.classList.add('image-container');
        hoverDiv.classList.add('hover-div');
        hoverDiv.addEventListener('click', removeMeme);
        const img = document.createElement('img');
        img.setAttribute('src', `${objectURL}`);
        newDiv.appendChild(img);
        newDiv.appendChild(hoverDiv);
        memeContainer.appendChild(newDiv);
        if(textTop) addTextTop(textTop);
        if(textBottom) addTextBottom(textBottom);
    });
}

function addTextTop(text){
    const textDiv = document.createElement('span');
    const textNode = document.createTextNode(text);
    textDiv.appendChild(textNode);
    textDiv.classList.add('text-top');
    memeContainer.lastChild.appendChild(textDiv);
}
function addTextBottom(text){
    const textDiv = document.createElement('span');
    const textNode = document.createTextNode(text);
    textDiv.appendChild(textNode);
    textDiv.classList.add('text-bottom');
    memeContainer.lastChild.appendChild(textDiv);
}
function removeMeme(event){
    memeContainer.removeChild(event.target.parentElement);
}
